//Copyright (C) 2015 JingJing
var httpoptions = {
    host: 'acc.ledwifi.de',
    port: 80,
    path: '/config',
    method: 'GET',
    headers: {
        'Content-Type': 'application/json'
    }
};
var http = require("http");
//var version = process.argv[2];
var version = process.env.carambolaver;
function find (arr, key, val) 
{
  for (var ai, i = arr.length; i--;)
    if ((ai = arr[i]) && ai[key] == val)
      return ai;
  return null;
}
function compareVersions(v1, comp, v2) {
    "use strict";
    var v1parts = v1.split('.'), v2parts = v2.split('.');
    var maxLen = Math.max(v1parts.length, v2parts.length);
    var part1, part2;
    var cmp = 0;
    for(var i = 0; i < maxLen && !cmp; i++) {
        part1 = parseInt(v1parts[i], 10) || 0;
        part2 = parseInt(v2parts[i], 10) || 0;
        if(part1 < part2)
            cmp = 1;
        if(part1 > part2)
            cmp = -1;
    }
    return eval('0' + comp + cmp);
}
var sys=require('sys');
var exec=require('child_process').exec;
var url="/usr/bin/m2mupgradefirm ";
function puts(error,stdout,stderr){sys.puts(stdout);}
var req = http.request(httpoptions, function(res)
{
    var output = '';
    console.log(httpoptions.host + ':' + res.statusCode);
    res.setEncoding('utf8');

    res.on('data', function (chunk) {
        output += chunk;
    });

    res.on('end', function() {
        var obj = JSON.parse(output);
        var caram = find(obj, 'name', 'image-carambola');
        console.log(obj); //delete
        if (caram!=null)
        {
            if (compareVersions(caram.version,'>', version)){
              url+="1 " + caram.path + " " + caram.hash;
              exec(url);
	    }
        }
    });
});

req.on('error', function(err) {
    //res.send('error: ' + err.message);
});
req.end();